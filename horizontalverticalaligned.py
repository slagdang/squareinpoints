#!/usr/bin/env python3

import sys

points = []

for line in sys.stdin:
    [x,y] = [int(a) for a in line.split(',')]
#    print(x,' ',y,' ',x+y)
    xdistances = [(x2-x)*(x2-x) for (x2,y2) in points if y2==y]
    ydistances = [(y2-y)*(y2-y) for (x2,y2) in points if x2==x]
    sqdistances = [2*(x2-x)*(x2-x) for (x2,y2) in points if (x2-x) == y2-y]
    for x in xdistances:
        if x in ydistances and 2*x in sqdistances:
            print('yep')
#    print(xdistances,ydistances,sqdistances)
    points.append([x,y])
